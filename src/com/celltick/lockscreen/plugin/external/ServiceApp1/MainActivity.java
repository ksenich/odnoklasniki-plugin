package com.celltick.lockscreen.plugin.external.ServiceApp1;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;

public class MainActivity extends Activity {

	private static final String PACKAGE_NAME = "com.celltick.lockscreen";
    private static final String PACKAGE_NAME_OK = "ru.ok.android";

    @Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		if (isInstalled(this, "com.celltick.lockscreen")) {
			open(this, "com.celltick.lockscreen.STARTLOCKER", PACKAGE_NAME);
			finish();
		}
		else {
			showDialog(this, PACKAGE_NAME);
		}

	}

    void doOk(){
        if (isInstalled(this, "ru.ok.android")) {
            open(this, "ru.ok.android", PACKAGE_NAME_OK);
            finish();
        }
        else {
            showDialog(this, PACKAGE_NAME_OK);
        }
    }

	static boolean isInstalled(Context context, String packageName ) {
		try {
			context.getPackageManager().getApplicationInfo(packageName, 0);
			return true;
		}
		catch (PackageManager.NameNotFoundException e) {
			return false;
		}
	}

    static void open(Activity activity, String appName, String packName) {
		try {
			Intent startApp = new Intent(appName);
			startApp.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			startApp.putExtra("origin", activity.getPackageName());
			startApp.putExtra("apk_type", "PLUGIN");
			activity.startActivity(startApp);
		}
		catch (Exception ex) {
			Intent startApp = activity.getPackageManager().getLaunchIntentForPackage(packName);
			startApp.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			startApp.putExtra("origin", activity.getPackageName());
			startApp.putExtra("apk_type", "THEME");
			activity.startActivity(startApp);
		}
	}

	private static void showDialog(final Activity activity, final String packageName) {
		AlertDialog.Builder builder = new AlertDialog.Builder(activity);
		builder.setTitle(R.string.go_to_gp_dialog_title);
		builder.setMessage(R.string.go_to_gp_dialog_message);
		builder.setPositiveButton(R.string.go_to_gp_dialog_yes, new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				goToGooglePlay(activity, packageName);
				activity.finish();
			}
		});
		builder.setNegativeButton(R.string.go_to_gp_dialog_no, new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				activity.finish();
			}
		});
		builder.show();
	}

     static void goToGooglePlay(Context activity, String packageName) {

		try {
			Uri marketUri = Uri.parse("market://details?id=" + packageName);

			Intent marketIntent = new Intent(Intent.ACTION_VIEW, marketUri);
			marketIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			activity.startActivity(marketIntent);
		}
		catch (Exception ex) {
			Uri browseUri = Uri.parse("https://play.google.com/store/apps/details?id=" + packageName);
			Intent browseIntent = new Intent(Intent.ACTION_VIEW, browseUri);
			browseIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			activity.startActivity(browseIntent);
		}
	}

}
