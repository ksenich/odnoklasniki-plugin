package com.celltick.lockscreen.plugin.external.ServiceApp1;


import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import ru.ok.android.sdk.Odnoklassniki;
import ru.ok.android.sdk.OkTokenRequestListener;

/**
 * Created with IntelliJ IDEA.
 * User: Offerb
 * Date: 19/01/14
 * Time: 11:43
 * To change this template use File | Settings | File Templates.
 */
public class ConfigActivity extends Activity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        Odnoklassniki ok = Odnoklassniki.getInstance(this);
        setContentView(R.layout.config);
        if(!ok.hasAccessToken()){
            Button button = (Button) findViewById(R.id.logout_button);
            button.setText(getString(R.string.login));
        }

    }

    public void logout(View view){
        Odnoklassniki ok = Odnoklassniki.getInstance(this);
        if(ok.hasAccessToken()){
            ok.clearTokens(this);
            Button button = (Button) findViewById(R.id.logout_button);
            button.setText(getString(R.string.login));
        }else{
            ok.requestAuthorization(this);
            ok.setTokenRequestListener(new OkTokenRequestListener() {
                @Override
                public void onSuccess(String s) {
                    Button button = (Button) findViewById(R.id.logout_button);
                    button.setText(getString(R.string.logout));
                    finish();
                }

                @Override
                public void onError() {

                }

                @Override
                public void onCancel() {

                }
            });
        }
    }


}
