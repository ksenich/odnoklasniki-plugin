package com.celltick.lockscreen.plugin.external.ServiceApp1;
import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class OkNotFoundActivity extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(MainActivity.isInstalled(this, "ru.ok.android")){
            MainActivity.open(this, "ru.ok.android.ui.activity.main.OdklActivity","ru.ok.android");
            finish();
        }else{
            setContentView(R.layout.install_ok);
            TextView tv = (TextView) findViewById(R.id.explanation_tv);
            tv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    install(view);
                }
            });
        }
    }

    public void install(View v){
        Log.d("install", "called");
        MainActivity.goToGooglePlay(this,"ru.ok.android");
        Toast.makeText(this,"woop",Toast.LENGTH_LONG);
        finish();
    }
}
