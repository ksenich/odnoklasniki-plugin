package com.celltick.lockscreen.plugin.external.ServiceApp1;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.net.Uri.Builder;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcelable;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.celltick.start.api.ApiValidator;
import com.celltick.start.api.PluginSettingProvider;
import com.celltick.start.api.StartContentProvider;
import com.celltick.start.api.message.BundleKey;
import com.celltick.start.api.message.ContentTemplate;
import com.celltick.start.api.messenger.StartMessenger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.ocpsoft.prettytime.PrettyTime;
import ru.ok.android.sdk.Odnoklassniki;

import java.io.*;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.*;

public class OdnoklassnikiStartPluginService extends Service implements StartContentProvider, PluginSettingProvider {

    private static final String ITEM_TEXT_COLOR = "#000000";
    private static final String ITEM_BG_COLOR = "#fdf5ea";
    private static final String ITEM_TITLE_COLOR = "#ee6600";
    private static final String ITEM_SUB_COLOR = "#c4d1a3";
    private static final String HEADER_BG_COLOR = "";
    private static final Random PRNG = new Random();
    private final StartMessenger startMessenger = StartMessenger.getInstance();
    private ArrayList<Bundle> bundles = new ArrayList<Bundle>();

    public OdnoklassnikiStartPluginService() {
        startMessenger.setStartContentProvider(this);
        startMessenger.setPluginSettingProvider(this);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return startMessenger.getBinder();
    }

    @Override
    public Bundle[] getContentResponse() {
        Log.d("getContentresponse", "entered");
        // getExternalContent()
        Bundle[] bundles;
        Odnoklassniki ok = Odnoklassniki.getInstance(getApplicationContext());
        if (ok.hasAccessToken()) {
            if (this.bundles.isEmpty()) {
                updateContent();
            }
            bundles = this.bundles.toArray(new Bundle[0]);
            for(int i=0;i<bundles.length;++i){
                Log.d("Bundles with photos", i + " " + bundles[i].containsKey("imageUrlArray"));
            }
        } else {
            Bundle loginBundle = new Bundle();
            loginBundle.putString(BundleKey.TITLE_1, "Login");
            loginBundle.putString(BundleKey.BACKGROUND_COLOR, "#ee6600");
            loginBundle.putParcelable(BundleKey.BACKGROUND_IMAGE, BitmapFactory.decodeResource(this.getResources(), R.drawable.logo_login));
            Uri.Builder builder = new Builder();
            Context context = getApplicationContext();
            if (context != null) {
                Log.d("getContentresponse", "context not null");
                builder
                        .scheme(context.getString(R.string.scheme))
                        .authority(context.getString(R.string.host))
                        .path(context.getString(R.string.path_login));
                loginBundle.putParcelable(BundleKey.ON_CLICK_LINK, builder.build());
            } else {
                Log.d("getContentresponse", "Context not found");
            }
            bundles = new Bundle[]{loginBundle};
        }
        try {
            return ApiValidator.validateContentResponse(bundles);
        } catch (InvalidObjectException e) {
            e.printStackTrace();
            Log.d("Invalid bundles", e.getMessage());
            return new Bundle[0];
        }
    }


    private void setupItemBundle(Bundle otherBundle) {
        otherBundle.putString(BundleKey.TITLE_1_COLOR, ITEM_TITLE_COLOR);
        otherBundle.putString(BundleKey.SUBTITLE_COLOR, ITEM_SUB_COLOR);
        otherBundle.putString(BundleKey.BACKGROUND_COLOR, ITEM_BG_COLOR);
        otherBundle.putString(BundleKey.ITEM_TEXT_COLOR, ITEM_TEXT_COLOR);
    }

    @Override
    public void updateContent() {
        Log.d("UpdateContent", "1");
        getConversations();
    }

    @Override
    public Bundle getPluginSettingInfo() {
        Bundle contentBundle = new Bundle();
        contentBundle.putString(BundleKey.TITLE_1, "Odnoklassniki");
        contentBundle.putString(BundleKey.ITEM_TEXT, getString(R.string.plugin_description));
        contentBundle.putString(BundleKey.TITLE_1_COLOR, "#ee6600");
        contentBundle.putInt(BundleKey.CONTENT_TEMPLATE, ContentTemplate.TEMPLATE_1);
        contentBundle.putParcelable(BundleKey.STARTER_ICON_1,
                BitmapFactory.decodeResource(
                        this.getResources(), R.drawable.icon_ok)
        );
        contentBundle.putParcelable(BundleKey.STARTER_ICON_2,
                BitmapFactory.decodeResource(
                        this.getResources(), R.drawable.icon_ok_color)
        );
        contentBundle.putParcelable(BundleKey.SETTINGS_ICON,
                BitmapFactory.decodeResource(
                        this.getResources(), R.drawable.icon_ok_color)
        );
        contentBundle.putString(BundleKey.BACKGROUND_COLOR, "#000000");
        Uri.Builder builder = new Uri.Builder();
        builder
                .scheme(getString(R.string.scheme))
                .authority(getString(R.string.host))
                .path(getString(R.string.path_config));
        contentBundle.putParcelable(BundleKey.ON_CLICK_LINK, builder.build());

        try {
            return ApiValidator.validatePluginSettingInfo(contentBundle);
        } catch (InvalidObjectException e) {
            e.printStackTrace();
            assert false;
            return contentBundle;
        }
    }

    void getConversations() {
        bundles.clear();
        new AsyncTask<Void, Void, String>() {

            @Override
            protected String doInBackground(Void... voids) {
                try {
                    ArrayList<Discussion> r = new ArrayList<Discussion>();
                    Map<String, String> params = new HashMap<String, String>();
//                  params.put("fields","object_type,object_id,message,new_comments_count,last_activity_date");
                    String result = Odnoklassniki
                            .getInstance(getApplicationContext())
                            .request("discussions.getList", params, "get");
                    return result;
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return "{}";
            }

            //TODO: use progress??
            @Override
            protected void onPostExecute(String result) {
                try {
                    Log.d("jsonDiscussions", result);
                    JSONObject responseJO = (JSONObject) new JSONTokener(result).nextValue();
                    JSONArray jarr = responseJO.getJSONArray("discussions");
                    JSONObject entitiesJO = responseJO.getJSONObject("entities");
                    for (int i = 0; i < jarr.length(); ++i) {
                        JSONObject jo = jarr.getJSONObject(i);
                        String object_type = jo.getString("object_type");
                        String object_id = jo.getString("object_id");
                        downloadComments(object_type, object_id, entitiesJO);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
//                startMessenger.notifyContentChange();
            }

        }.execute();
    }

    private void downloadComments(final String object_type, final String object_id, final JSONObject entitiesJO) {
        new AsyncTask<Void, Void, String>() {

            @Override
            protected String doInBackground(Void... voids) {
                Odnoklassniki ok = Odnoklassniki.getInstance(getApplicationContext());
                Map<String, String> params = new HashMap<String, String>();
                params.put("discussionId", object_id);
                params.put("discussionType", object_type);
                try {
                    String result = ok.request("discussions.getComments", params, "get");
                    return result;
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(String result) {
                try {
                    Log.d("jsonComments", result);
                    JSONObject resObj = (JSONObject) new JSONTokener(result).nextValue();
                    JSONArray commentsArray = resObj.getJSONArray("comments");
                    for (int i = 0; i < commentsArray.length(); ++i) {
                        Bundle b = new Bundle();
                        b.putParcelable(BundleKey.ON_CLICK_LINK, new Builder()
                                .scheme(getString(R.string.scheme))
                                .authority(getString(R.string.host))
                                .path(getString(R.string.path_ok_not_found))
                                .build());
//                        renderCommentBundle(b, object_type, object_id, commentsArray.getJSONObject(i), entitiesJO);
                        constructCommentBundle(b, object_type, object_id, commentsArray.getJSONObject(i), entitiesJO);
                        Log.d("Adding bundle", b.toString());
                        bundles.add(b);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
//                startMessenger.notifyContentChange();
            }
        }.execute();
    }

    private Date stringToDate(String aDate, String aFormat) {
        if (aDate == null) return null;
        ParsePosition pos = new ParsePosition(0);
        SimpleDateFormat simpledateformat = new SimpleDateFormat(aFormat);
        TimeZone msk = TimeZone.getTimeZone("GMT+03:00");
        TimeZone here = TimeZone.getDefault();
        long offset = msk.getRawOffset() - here.getRawOffset();
        Date stringDate = simpledateformat.parse(aDate, pos);
        stringDate.setTime(stringDate.getTime() - offset);
        return stringDate;
    }

    private Bitmap renderComment(JSONObject jo, JSONObject entitiesJO) {
        View b = View.inflate(this, R.layout.stream_item, null);
        try {
            Map<String, JSONObject> photosByID = new HashMap<String, JSONObject>();
            if (entitiesJO.has("user_photos")) {
                JSONArray photosJA = entitiesJO.getJSONArray("user_photos");
                for (int i = 0; i < photosJA.length(); ++i) {
                    JSONObject photoJO = photosJA.getJSONObject(i);
                    photosByID.put(photoJO.getString("id"), photoJO);
                }
            }
            Map<String, JSONObject> usersById = new HashMap<String, JSONObject>();
            if (entitiesJO.has("users")) {
                JSONArray photosJA = entitiesJO.getJSONArray("users");
                for (int i = 0; i < photosJA.length(); ++i) {
                    JSONObject photoJO = photosJA.getJSONObject(i);
                    usersById.put(photoJO.getString("uid"), photoJO);
                }
            }

            TextView nameView = (TextView) b.findViewById(R.id.name);
            TextView timeView = (TextView) b.findViewById(R.id.time_passed);
            TextView commentView = (TextView) b.findViewById(R.id.comment);
            ImageView upicView = (ImageView) b.findViewById(R.id.user_pic);
            ImageView illustrationView = (ImageView) b.findViewById(R.id.illustration);
            JSONObject userJO = usersById.get(jo.getString("author_id"));
            if(userJO!=null){
                nameView.setText(userJO.getString("name"));
            }
            timeView.setText(timePassedSince(jo.getString("date")));
            commentView.setText(jo.getString("text"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Bitmap bitmap = renderViewToBitmap(b);
        return bitmap;
    }

    private Bitmap renderViewToBitmap(View b) {
        b.setDrawingCacheEnabled(true);
        DisplayMetrics displaymetrics = getResources().getDisplayMetrics();
        int height = displaymetrics.heightPixels;
        int width = displaymetrics.widthPixels;
        b.layout(0, 0, 100, width);
        b.buildDrawingCache();
        Bitmap bitmap = Bitmap.createBitmap(b.getDrawingCache());
        b.setDrawingCacheEnabled(false);
        return bitmap;
    }

    private void renderCommentBundle(Bundle b, String object_type, String object_id, JSONObject jo, JSONObject entitiesJO) {
        b.putParcelable(BundleKey.BACKGROUND_IMAGE, getImageUri(this, renderComment(jo, entitiesJO), object_id));

    }

    private void constructCommentBundle(Bundle b, String object_type, String object_id, JSONObject jo, JSONObject entitiesJO) {
        setupItemBundle(b);
        try {
            Map<String, JSONObject> photosByID = new HashMap<String, JSONObject>();
            if (entitiesJO.has("user_photos")) {
                JSONArray photosJA = entitiesJO.getJSONArray("user_photos");
                for (int i = 0; i < photosJA.length(); ++i) {
                    JSONObject photoJO = photosJA.getJSONObject(i);
                    photosByID.put(photoJO.getString("id"), photoJO);
                }
            }
            Map<String, JSONObject> usersById = new HashMap<String, JSONObject>();
            if (entitiesJO.has("users")) {
                JSONArray usersJA = entitiesJO.getJSONArray("users");
                for (int i = 0; i < usersJA.length(); ++i) {
                    JSONObject userJO = usersJA.getJSONObject(i);
                    usersById.put(userJO.getString("uid"), userJO);
                }
            }
            b.putString(BundleKey.ITEM_TEXT, jo.getString("text"));
            String dateString = jo.getString("date");
            String fs = timePassedSince(dateString);
            b.putString(BundleKey.SUBTITLE, fs);
            if (object_type.equals("USER_PHOTO")) {
                Parcelable[] urls_array = new Parcelable[]{Uri.parse(photosByID.get(object_id).getString("pic128x128"))};
                b.putParcelableArray(BundleKey.IMAGE__URL_ARRAY, urls_array);
                Log.d("Bundle has photo", String.valueOf(bundles.indexOf(b)) +" " + b.toString().contains("imageUrlArray"));
            }
//            putInfoOnAuthor(b,jo.getString("author_id"));
            JSONObject resObj = usersById.get(jo.getString("author_id"));
            b.putString(BundleKey.TITLE_1, resObj.getString("last_name") + " " + resObj.getString("first_name"));
            b.putParcelable(BundleKey.IMAGE_URL, Uri.parse(resObj.getString("pic128x128")));
            Log.d("Bundle finalized", String.valueOf(bundles.indexOf(b)) + " " + b.toString().contains("imageUrlArray"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public Uri getImageUri(Context inContext, Bitmap inImage, String title) {
//        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
//        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, title, "whatever");
        try {
            File f = File.createTempFile(title, "jpg");
            FileOutputStream out = new FileOutputStream(f);
            inImage.compress(Bitmap.CompressFormat.JPEG, 100, out);
            return Uri.parse(f.toURI().toString());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new Builder().build();
    }

    private String timePassedSince(String dateString) {
        String dateFormat = "yyyy-MM-dd hh:mm:ss";
        PrettyTime pt = new PrettyTime();
        return pt.format(stringToDate(dateString, dateFormat));
    }

    private void putInfoOnAuthor(final Bundle b, final String author_id) {
        new AsyncTask<Void, Void, String>() {

            @Override
            protected String doInBackground(Void... voids) {
                Odnoklassniki ok = Odnoklassniki.getInstance(getApplicationContext());
                try {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("uids", author_id);
                    params.put("fields", "name,pic_5");
                    String result = ok.request("users.getInfo", params, "get");
                    return result;
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(String result) {
                try {
                    JSONObject resObj = ((JSONArray) new JSONTokener(result).nextValue()).getJSONObject(0);
                    b.putString(BundleKey.TITLE_1, resObj.getString("name"));
                    b.putParcelable(BundleKey.IMAGE_URL, Uri.parse(resObj.getString("pic128x128")));
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                startMessenger.notifyContentChange();
            }

        }.execute();
    }

    static class Photo {
        public String user_id;
        String url_128x128;
        String url_50x50;
        String id;
    }

    class Friend {
        String name;
        String pic_5;
    }

    class Convo{
        String uid;
        int new_msgs_count;
        String last_msg_txt;
        String last_msg_time;
    }

    class Event{
        String uid;
        String type;
        int number;
        String lastID;
        String message;
        String icon;
    }

    class Discussion {
        public String photo;
        String whole;
        String object_type;
        String object_id;
        String last_action_date;
        String message;
        int unread_messages;
    }

    class Comment {
        Friend author;
        String text;
        Date added;

    }
}
