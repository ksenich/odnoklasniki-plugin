package com.celltick.lockscreen.plugin.external.ServiceApp1;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;
import ru.ok.android.sdk.Odnoklassniki;
import ru.ok.android.sdk.OkTokenRequestListener;
import ru.ok.android.sdk.util.OkScope;

public class LoginActivity extends Activity{
    private static final String APP_ID = "1093766400";
    private static final String PUBLIC_KEY = "CBAOICDCEBABABABA";
    private static final String PRIVATE_KEY = "FA54218EE19120F5A1E171B8";
    protected Odnoklassniki ok;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ok = Odnoklassniki.getInstance(this);
        ok.requestAuthorization(this, false, OkScope.VALUABLE_ACCESS);
        ok.setTokenRequestListener(new OkTokenRequestListener() {
            @Override
            public void onSuccess(final String accessToken) {
                Toast.makeText(LoginActivity.this, "Recieved token : " + accessToken, Toast.LENGTH_SHORT).show();
                Log.d("loginSuccess", "Recieved token : " + accessToken);
                finish();
            }

            @Override
            public void onCancel() {
                Toast.makeText(LoginActivity.this, "Authorization was canceled", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onError() {
                Toast.makeText(LoginActivity.this, "Error getting token", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void login(View view){
    }
}
